# Lightning Login System

## Introduction
Lightning is a highly opinionated login module that provides functions and methods to verify the identity of 
connecting individuals.

It supports Multi-factor authorization, password hashing and API Token management.

## Integrations
* Google Authenticator
* Mongoose
* Twilio
* Koa

## Documentation
Documentation is provided on Read the Docs at the following link.

## Test Suite
Lightning is fully tested using Mocha. Just remember that lightning is a module build with Generators in mind, 
so harmony is required.

## Contact 
If for any reason you would like to contact the maintainers of this project please feel free to email the leader of 
our S1 department at striker.a@23rd-div.com or join our teamspeak at ts.23rd-div.com

## Donations
Lightning is maintained by the S1 team from the 23rd Gaming Division ARMA III clan. We strive to present a 
professional service with our software, however we do all of our programming in our free time out of love for our group.

If you enjoy using our software, perhaps you would consider a donation to the 23rd Gaming Division, 
or to Help the Heroes.

